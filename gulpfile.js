var path = {
  watch: {
    html: 'assets/src/**/*.html',
    js: 'assets/src/js/**/*.js',
    css: 'assets/src/css/**/*.scss',
    img: 'assets/src/img/**/*.*',
    fonts: 'assets/srs/fonts/**/*.*'
  },
  src: {
    html: 'assets/src/*.html',
    js: 'assets/src/js/main.js',
    css: 'assets/src/css/main.scss',
    img: 'assets/src/img/**/*.*',
    fonts: 'assets/src/fonts/**/*.*'
  },
  build: {
    html: 'assets/build/',
    js: 'assets/build/js/',
    css: 'assets/build/css/',
    img: 'assets/build/img/',
    fonts: 'assets/build/fonts/'
  },
  clean: './assets/build/*'
};

const gulp = require('gulp'),
      webserver = require('browser-sync'),
      plumber = require('gulp-plumber'),
      rigger = require('gulp-rigger'),
      sourcemaps = require('gulp-sourcemaps'),
      sass = require('gulp-sass'),
      autoprefixer = require('gulp-autoprefixer'),
      cleanCSS = require('gulp-clean-css'),
      uglify = require('gulp-uglify'),
      cache = require('gulp-cache'),
      imagemin = require('gulp-imagemin'),
      jpegrecompress = require('gulp-jpeg-recompress'),
      pngquant = require('imagemin-pngquant'),
      rimraf = require('gulp-rimraf'),
      rename = require('gulp-rename'),
      watch = require('gulp-watch');

//сборка html
gulp.task('html:build', function() {
  return gulp.src(path.src.html)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(webserver.reload({ stream: true }));
});

//сборка css
gulp.task('css:build', function() {
  return gulp.src(path.src.css) // получим main.scss
    .pipe(plumber()) // для отслеживания ошибок
    .pipe(sourcemaps.init()) // инициализируем sourcemap
    .pipe(sass()) // scss -> css
    .pipe(gulp.dest(path.build.css))
    .pipe(rename({ suffix: '.min' }))
    .pipe(cleanCSS()) // минимизируем CSS
    .pipe(sourcemaps.write('./')) // записываем sourcemap
    .pipe(gulp.dest(path.build.css)) // выгружаем в build
    .pipe(webserver.reload({ stream: true })); // перезагрузим сервер
});

//сборка js
gulp.task('js:build', function() {
  return gulp.src(path.src.js)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(path.build.js))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(path.build.js))
    .pipe(webserver.reload({ stream: true }));
});

//копирование шрифтов
gulp.task('fonts:build', function() {
  return gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts));
});

//обработка изображений
gulp.task('img:build', function() {
  return gulp.src(path.src.img)
    .pipe(cache(imagemin([ // сжатие изображений
      imagemin.gifsicle({ interlaced: true }),
      jpegrecompress({
        progressive: true,
        max: 90,
        min: 80
      }),
      pngquant(),
      imagemin.svgo({ plugins: [{ removeViewBox: false }] })
    ])))
    .pipe(gulp.dest(path.build.img));
});

//очистка папки build
gulp.task('clean:build', function() {
  return gulp.src(path.clean, {read: false})
    .pipe(rimraf());
});

//очистка кэша
gulp.task('cache:clear', function() {
  cache.clearAll();
});

//сборка проекта
gulp.task('build',
  gulp.series('clean:build',
    gulp.parallel(
      'html:build',
      'css:build',
      'js:build',
      'img:build',
      'fonts:build'
    )
  )
);

gulp.task('webserver', function() {
  webserver({
    server: {
      baseDir: './assets/build'
    },
    notify: false,
    open: false,
    host: "0.0.0.0",
    port: 8000
  });
});

gulp.task('watch', function() {
  watch(path.watch.html, gulp.series('html:build'));
  watch(path.watch.css, gulp.series('css:build'));
  watch(path.watch.js, gulp.series('js:build'));
  watch(path.watch.img, gulp.series('img:build'));
  watch(path.watch.fonts, gulp.series('fonts:build'));
});

gulp.task('default', gulp.series('build', gulp.parallel('webserver', 'watch')));


// ================== Старый gulpfile.js

// const gulp = require('gulp'),
//       sass = require('gulp-sass'),
//       sourcemaps = require('gulp-sourcemaps'),
//       watch = require('gulp-watch'),
//       webserver = require('gulp-webserver');

// gulp.task('sass', function() {
//     return gulp.src(['app/sass/**/*.sass', 'app/sass/**/*.scss'])
//     .pipe(sourcemaps.init())
//     .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
//     .pipe(sourcemaps.write('./'))
//     .pipe(gulp.dest('app/css/'))
// })

// gulp.task('watch', function() {
//   gulp.watch(['app/sass/**/*.sass', 'app/sass/**/*.scss'], gulp.series('sass'));
// });

// // gulp.task('default', ['watch']);

// gulp.task('webserver', function() {
//     gulp.src('./app/')
//     .pipe(webserver({
//         host: '0.0.0.0',
//         port: '8000',
//         livereload: true,
//         directoryListing: true,
//         open: true,
//         // fallback: 'index.html'
//     }));
// });